<header id="top"></header>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/bitterdock/recreational-flight-planner">
    <img src="logo.png" alt="Logo" width="200" height="200">
  </a>

<h3 align="center">Recreational Flight Planner</h3>

  <p align="center">
    An application for recreational pilots to plan flights, so they are less likely to be involved in fuel exhaustion events.
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-project">About Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#running">Running</a></li>
        <li><a href="#tests">Tests</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
  </ol>
</details>

<!-- ABOUT PROJECT -->
## About Project

Recreational Flight Planner is an application for recreational pilots to plan flights, so they are less likely to be involved in fuel exhaustion events.
The user can use imported/created airport and aircraft data to plan flights that are within range of their aircraft.
Airline, route and flight data can be imported and viewed in a raw data viewer.

Recreational Flight Planner was created by Johnny Howe, Niko Tainui, Troy Tomlins, Mitchell Freeman, Tom Rizzi, and Zachary Kaye
for SENG202.

<div align="right">(<a href="#top">back to top</a>)</div>

### Built With

[![Apache Maven][ApacheMaven-badge]][ApacheMaven-url]
[![GitLab CI][GitLabCI-badge]][GitLabCI-url]
[![Google Maps][GoogleMaps-badge]][GoogleMaps-url]
[![Intellij IDEA][IntellijIDEA-badge]][IntellijIDEA-url]
[![Java][Java-badge]][Java-url]
[![JavaScript][JavaScript-badge]][JavaScript-url]
[![SQLite][SQLite-badge]][SQLite-url]

<div align="right">(<a href="#top">back to top</a>)</div>

<!-- GETTING STARTED -->
## Getting Started

To set up and run Recreational Flight Planner locally follow the steps below.

### Prerequisites

Before installing and running Recreational Flight Planner the following is required:
* Java - (there is a minimum requirement of Java 11) \
https://www.oracle.com/nz/java/technologies/javase/jdk11-archive-downloads.html | https://jdk.java.net/11/
* Maven - https://maven.apache.org/

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/bitterdock/recreational-flight-planner
   ```
2. Open the project in a terminal
3. Generate the .jar to be run
   ```sh
   mvn clean package
   ```

### Running

1. Run Recreational Flight Planner
   ```sh
   java -jar target/RecreationalFlightPlanner-1.0-SNAPSHOT.jar
   ```

**OR**

1. Build and run Recreational Flight Planner using your favourite IDE, e.g. Jetbrains IntelliJ IDEA

### Tests

1. Run the tests using your favourite IDE, e.g. Jetbrains IntelliJ IDEA

<div align="right">(<a href="#top">back to top</a>)</div>


<!-- USAGE EXAMPLES -->
## Usage

A user manual (`user-manual.pdf`) has been provided in the root of the project. The user manual gives instructions
on how to install and run the application, as well as how to use it.

Example data files, that can be imported, have been included in `src/main/resources/seng202/group10/view/example-data-files`.


<div align="right">(<a href="#top">back to top</a>)</div>

<!-- MARKDOWN LINKS & IMAGES -->
[GitLabCI-badge]: https://img.shields.io/badge/gitlab%20ci-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white
[GitLabCI-url]: https://docs.gitlab.com/ee/ci/
[SQLite-badge]: https://img.shields.io/badge/sqlite-%2307405e.svg?style=for-the-badge&logo=sqlite&logoColor=white
[SQLite-url]: https://www.sqlite.org/index.html
[IntellijIDEA-badge]: https://img.shields.io/badge/IntelliJIDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white
[IntellijIDEA-url]: https://www.jetbrains.com/idea/
[Java-badge]: https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=openjdk&logoColor=white
[Java-url]: https://www.java.com/en/
[JavaScript-badge]: https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E
[JavaScript-url]: https://www.javascript.com/
[ApacheMaven-badge]: https://img.shields.io/badge/Apache%20Maven-C71A36?style=for-the-badge&logo=Apache%20Maven&logoColor=white
[ApacheMaven-url]: https://maven.apache.org/
[GoogleMaps-badge]: https://img.shields.io/static/v1?style=for-the-badge&message=Google+Maps&color=4285F4&logo=Google+Maps&logoColor=FFFFFF&label=
[GoogleMaps-url]: https://developers.google.com/maps/documentation/javascript